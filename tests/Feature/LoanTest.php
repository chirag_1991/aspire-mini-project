<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\LoanRepayment;
use App\Dictionaries\Loan\LoanActionDictionary;
use Carbon\Carbon;

use Tests\TestCase;

/**
 * class LoanTest
 *
 * @package Tests\Feature
 */
class LoanTest extends TestCase
{
    use DatabaseTransactions;

    private static $loan;

    /**
     * Loan Application feature test, authentication.
     *
     * @return void
     */
    public function testOnlyAuthenticateUserCanApplyForLoan()
    {

        $this->json('POST', 'api/loan-application',['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                "message" =>  "Unauthenticated."
            ]);
    }

    /**
     * Loan Application feature test, mandatory fields.
     *
     * @return void
     */
    public function testCheckMandatoryFieldsForLoanApplication()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $this->json('POST', 'api/loan-application',['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" =>  "The given data was invalid.",
                "errors" => [
                    "loan" => ["The loan field is required."],
                    "loan_term_id" => ["The loan term id field is required."]
                ]
            ]);
    }

    /**
     * Loan Application feature test, invalid loan data.
     *
     * @return void
     */
    public function testCheckLoanApplicationForInvalidInput()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loanData = [
            "loan" => "string", //non-numeric
            "loan_term_id" => 1000000 //in-valid term-id 
        ];
        $response = $this->json('POST', 'api/loan-application', $loanData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" =>  "The given data was invalid.",
                "errors" => [
                    "loan" => [
                        "The loan must be a number.",
                        "The loan must be at least 25000."
                    ],
                    "loan_term_id" => [
                        "The selected loan term id is invalid."
                    ]
                ]
        ]);
    }


    /**
     * Loan Application feature test, create a loan.
     *
     * @return void
     */
    public function testCreateLoanApplicationSuccesfully()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loanData = [
            "loan" => "50000", //non-numeric
            "loan_term_id" => 1 //in-valid term-id 
        ];
        $response = $this->json('POST', 'api/loan-application', $loanData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" =>  "You have successfully applied for a loan!"
        ]);
    }


    /**
     * Loan Application feature test, retrieve loan data successfully.
     *
     * @return void
     */
    public function testRetrieveLoanApplicationSuccesfully()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loan = \App\Models\Loan::factory()->create(
            ['user_id' => $user->id]
        );

        $response = $this->json('GET', 'api/loans', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "code" =>  "data_fetched",
                "result" => [
                    [
                        "id" => $loan->id,
                        "user_id" => $loan->user_id,
                        "loan" => $loan->loan,
                        "loan_term_id" => $loan->loan_term_id,
                        "total_interest_paid" => null,
                        "total_amount_paid" => null,
                        "repayment_frequency" => "week",
                        "action_user_id" => null,
                        "status" => "pending",
                        "comment" => null,
                        "loan_repayments" => []
                    ]
            ]
        ]);
    }

    /**
     * Repayment for the pending loan.
     *
     * @return void
     */
    public function testErrorWhileRepayingForPendingLoan()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loan = \App\Models\Loan::factory()->create(
            ['user_id' => $user->id]
        );

        $this->json('PATCH', 'api/loans/'. $loan->id .'/repayment'   , ['Accept' => 'application/json'])
            ->assertStatus(400)
            ->assertJson([
                "code" => "invalid_request",
                "message" =>  "Error Processing Request paying loan repayments: Error Processing Request paying loan repayments: Invalid Request"
        ]);
    }


    /**
     * Test function to validate mandatory field for loan approval.
     *
     * @return void
     */
    public function testCheckMandatoryFieldValidationForLoanApproval()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loan = \App\Models\Loan::factory()->create(
            ['user_id' => $user->id]
        );

        $user = User::find(1);
        $this->actingAs($user, 'api');

        $this->json('PATCH', 'api/loan-action/'. $loan->id, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" =>  "The given data was invalid.",
                "errors" => [
                    "action" => [
                        "The action field is required."
                    ]
                ]
        ]);
    }


    /**
     * Test function to check update the loan status.
     *
     * @return void
     */
    public function testApproveLoanStatusAndGenerateRepaymentDueDates()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loan = \App\Models\Loan::factory()->create(
            [
                'user_id' => $user->id,
                'loan_term_id' => 1
            ]
        );

        $adminUser = User::find(1);
        $this->actingAs($adminUser, 'api');

        $data = ['action' => LoanActionDictionary::APPROVE];

        $this->json('PATCH', 'api/loan-action/'. $loan->id, $data ,['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "message" =>  "Loan status updates successfully"
        ]);

        $this->actingAs($user, 'api');
        $loanRepayment = LoanRepayment::where('loan_id', $loan->id)->first();
        $nextDate = Carbon::now()->addDays(3)->format('Y-m-d');
        $loanAmount = round($loan->loan + ($loan->loan * config('loan.interest_rate') / 100), 2);
        $amount_paid = round($loanAmount / 4, 2);
        $response = $this->json('GET', 'api/loans', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "code" =>  "data_fetched",
                "result" => [
                    [
                        "id" => $loan->id,
                        "user_id" => $loan->user_id,
                        "loan" => $loan->loan,
                        "loan_term_id" => $loan->loan_term_id,
                        "total_interest_paid" => null,
                        "total_amount_paid" => null,
                        "repayment_frequency" => "week",
                        "action_user_id" => null,
                        "status" => "approve",
                        "comment" => null,
                        "loan_repayments" => [
                            [
                                'id' => $loanRepayment->id,
                                'loan_id' => $loan->id,
                                'due_date' => $nextDate,
                                'interest_rate' => null,
                                'amount_paid' => $amount_paid,
                                'interest_paid' => null,
                                'is_paid' => 0
                            ],
                            [
                                'id' => $loanRepayment->id + 1,
                                'loan_id' => $loan->id,
                                'due_date' => Carbon::now()->addDays(10)->format('Y-m-d'),
                                'interest_rate' => null,
                                'amount_paid' => $amount_paid,
                                'interest_paid' => null,
                                'is_paid' => 0
                            ],
                            [
                                'id' => $loanRepayment->id + 2,
                                'loan_id' => $loan->id,
                                'due_date' => Carbon::now()->addDays(17)->format('Y-m-d'),
                                'interest_rate' => null,
                                'amount_paid' => $amount_paid,
                                'interest_paid' => null,
                                'is_paid' => 0
                            ],
                            [
                                'id' => $loanRepayment->id + 3,
                                'loan_id' => $loan->id,
                                'due_date' => Carbon::now()->addDays(24)->format('Y-m-d'),
                                'interest_rate' => null,
                                'amount_paid' => $amount_paid,
                                'interest_paid' => null,
                                'is_paid' => 0
                            ]
                        ]
                    ]   
            ]
        ]);
    }


    /**
     * Test function to system should throw error if admin trying to update the status to someother than approve or reject.
     *
     * @return void
     */
    public function testAdminTryingToUpdateLoanStatusToInvalidStatus()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loan = \App\Models\Loan::factory()->create(
            ['user_id' => $user->id]
        );

        $user = User::find(1);
        $this->actingAs($user, 'api');

        $data = ['action' => 'test'];
        
        $this->json('PATCH', 'api/loan-action/'. $loan->id, $data ,['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" =>  "The given data was invalid.",
                "errors" => [
                    "action" => [
                        "The selected action is invalid."
                    ]
                ]
        ]);
    }


    /**
     * Test function repayment of approved loan
     *
     * @return void
     */
    public function testMakeRepaymentForApprovedLoan()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user, 'api');

        $loan = \App\Models\Loan::factory()->create(
            [
                'user_id' => $user->id,
                'status' => LoanActionDictionary::APPROVE
            ]
        );

        $loanTerms = \App\Models\LoanRepayment::factory()->create(
            [
                'loan_id' => $loan->id,
            ]
        );

        $this->json('PATCH', 'api/loans/'. $loan->id .'/repayment'   , ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "code" => "data_fetched",
                "message" =>  "Thank you for the repayment"
        ]);
    }
}
