<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * class AuthenticationTest
 *
 * @package Tests\Feature
 */
class AuthenticationTest extends TestCase
{
    use DatabaseTransactions;
    /**
    * Test method for reuired field for registration
    * @return mixed
    */
    public function testRequiredFieldsForRegistration()
    {
        $this->json('POST', 'api/register',['Accept' => 'application/json'])
            ->assertStatus(400)
            ->assertJson([
                "message" =>  [
                    "name" => ["The name field is required."],
                    "email" => ["The email field is required."],
                    "password" => ["The password field is required."]
                ],
                "code" => ''
            ]);
    }

    /**
    * Test method for registration
    * @return mixed
    */
    public function testSuccessfulRegistration()
    {
        $userData = [
            "name" => "Test user",
            "email" => "test-user@example.com",
            "password" => "test@123",
            "password_confirmation" => "test@123"
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "user" => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                ],
                "message"
            ]);
    }


    /**
    * Test method for login mandatory fields
    *
    * @return mixed
    */
    public function testMustEnterEmailAndPassword()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(400)
            ->assertJson([
                "message" => [
                    'email' => ["The email field is required."],
                    'password' => ["The password field is required."],
                ],
                "code" => ''
            ]);
    }

    /**
    * Test method for login
    *
    * @return mixed
    */
    public function testSuccessfulLogin()
    {

        $user = \App\Models\User::factory()->create([
           'email' => 'sample@test.com',
           'password' => bcrypt('sample123'),
        ]);


        $loginData = ['email' => 'sample@test.com', 'password' => 'sample123'];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
               "user" => [
                   'id',
                   'name',
                   'email',
                   'email_verified_at',
                   'created_at',
                   'updated_at',
               ],
                "access_token",
                "token_type",
                "expires_in"
            ]);

        $this->assertAuthenticated();
    }
}
