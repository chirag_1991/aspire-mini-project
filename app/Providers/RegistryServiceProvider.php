<?php

namespace App\Providers;

use App\Services\Loan\LoanService;
use App\Services\Loan\LoanServiceImpl;
use Illuminate\Support\ServiceProvider;

/**
 * class RegistryServiceProvider
 *
 * @package App\Providers
 */
class RegistryServiceProvider extends ServiceProvider
{

	/**
	 * Register services
	 *
	 * @return void
	*/
	public function register()
	{
		$this->app->singleton(LoanService::class, LoanServiceImpl::class);
	}

	/**
	 * Bootservices
	 *
	 * @return void
	*/
	public function boot()
	{
		//
	}
}