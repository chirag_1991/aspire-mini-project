<?php

namespace App\Exceptions;

use App\Utils\ResponseUtils;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

/**
 * Class InvalidDataException
 *
 * @package App\Exceptions
 */
class InvalidDataException extends Exception
{
    /**
     *
     */
    public function report()
    {
        Log::error($this->getMessage());
        Log::debug($this->getTraceAsString());
    }

    /**
     * @return JsonResponse
     */
    public function render()
    {
        return ResponseUtils::errorWithMessage($this->getMessage(), "invalid_request", 400);
    }
}