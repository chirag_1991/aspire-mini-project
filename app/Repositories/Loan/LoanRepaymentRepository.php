<?php

namespace App\Repositories\Loan;

use App\Models\LoanRepayment;
use App\Repositories\BaseRepository;

/**
 * class LoanRepaymentRepository
 *
 * @package App\Repositories
 */
class LoanRepaymentRepository extends BaseRepository
{
	/**
	* @var $model
	*/
	protected $model;

	/**
	 * LoanRepaymentRepository Constructor
	 *
	 * @param Loan $model
	*/
	public function __construct(LoanRepayment $model)
	{
		parent::__construct($model);
	}

	/**
	* Function to fetch users loan Term
	*
	* @return mixed
	*/
	public function getLoanRepayments(int $loanTermId) 
	{
		return $this->model->find($loanTermId);
	}

	/**
	* This method is to create loan repayment
	*	
	* @param array $data
	*
	* @return mixed
	*/
	public function createLoanRepayments(array $data) 
	{
		$objLoanRepayment = new $this->model;

		return $this->model->updateOrCreate([
			'loan_id' => $data['loan_id'], 
			'due_date' => $data['due_date']
		], 
		[
			'amount_paid' => $data['amount_paid']
		]);
		
	}

	/**
	* This methos is used for loan repayment
	*
	* @param int $loanId	
	* @param array $data	
	*
	* @return mixed
	*/
	public function updateLoanRepayment(int $loanId, array $data) 
	{
		$objLoanRepayment = $this->model
		->where('loan_id', $loanId)
		->where('is_paid', false)
		->first();

		if($objLoanRepayment){
			$objLoanRepayment->is_paid = true;
			$objLoanRepayment->interest_rate = $data['interest_rate'];
			//$objLoanRepayment->interest_paid = $data['interest_paid'];
			$objLoanRepayment->save();		

			return $objLoanRepayment;
		}
		return false;
	}
}