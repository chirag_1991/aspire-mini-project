<?php

namespace App\Repositories\Loan;

use App\Models\Loan;
use App\Repositories\BaseRepository;
use App\Dictionaries\Loan\LoanActionDictionary;

/**
 * class LoanRepository
 *
 * @package App\Repositories
 */
class LoanRepository extends BaseRepository
{

	/**
	* @var $model
	*/
	protected $model;

	/**
	 * LoanRepository Constructor
	 *
	 * @param Loan $model
	*/
	public function __construct(Loan $model)
	{
		parent::__construct($model);
	}


	/**
	* Function to fetch the active pending request
	*
	* @param int $userId
	*
	* @return mixed
	*/
	public function fetchActivePendingRequest(int $userId) 
	{
		
		return $this->model->where('user_id', $userId)->where('status', LoanActionDictionary::PENDING)->get();
	}

	/**
	* Function to save loan data.
	*
	* @param array $data
	*
	* @return mixed
	*/
	public function saveLoanData(array $data) 
	{
		
		$loanObj = new $this->model;

		$loanObj->user_id = $data['user_id'];
		$loanObj->loan = $data['loan'];
		$loanObj->loan_term_id = $data['loan_term_id'];
		$loanObj->repayment_frequency = $data['repayment_frequency'];
		$loanObj->status = $data['status'];

		return $loanObj->save();
	}

	/**
	* Function to fetch users loan data
	*
	* @param int $userId
	*
	* @return mixed
	*/
	public function getUserLoans(int $userId) 
	{
		return $this->model->where('user_id', $userId)->with('loanRepayments')->get();
	}

	/**
	* This method is to update the loan status
	*
	* @param int $loanId	
	* @param array $data
	*
	* @return mixed
	*/
	public function updateLoanStatus(int $loanId, array $data) 
	{
		$objLoan = $this->getLoan($loanId);
		$objLoan = $objLoan->first();

		$objLoan->status = $data['action'];
		return $objLoan->save();
	}

	/**
	* Function to fetch users loan by loan Id
	*
	* @param int $loanId
	*
	* @return mixed
	*/
	public function getLoan(int $loanId) 
	{
		return $this->model->where('id', $loanId)->where('status', LoanActionDictionary::PENDING);
	}


	/**
	* Function to fetch users loan data
	*
	* @param int $loanId
	* @param int $userId
	*
	* @return mixed
	*/
	public function getUserLoanById(int $loanId, int $userId) 
	{
		return $this->model
		->where('id', $loanId)
		->where('user_id', $userId)
		->where('status', LoanActionDictionary::APPROVE)
		->first();
	}


	/**
	* This method is to update the loan amount
	*
	* @param int $loanId	
	* @param array $amounts
	*
	* @return mixed
	*/
	public function updateLoanAmount(int $loanId, array $amounts) 
	{
		$objLoan = $this->model->where('id', $loanId)->first();
		
		$objLoan->total_amount_paid = $objLoan->total_amount_paid + $amounts['amount_paid'];
		return $objLoan->save();
	}
}