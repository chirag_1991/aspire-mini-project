<?php

namespace App\Repositories\Loan;

use App\Models\LoanTerm;
use App\Repositories\BaseRepository;

/**
 * class LoanTermRepository
 *
 * @package App\Repositories
 */
class LoanTermRepository extends BaseRepository
{
	/**
	* @var $model
	*/
	protected $model;

	/**
	 * LoanRepository Constructor
	 *
	 * @param Loan $model
	*/
	public function __construct(LoanTerm $model)
	{
		parent::__construct($model);
	}

	/**
	* Function to fetch users loan Term
	*
	* @return mixed
	*/
	public function getLoanTerm(int $loanTermId) 
	{
		return $this->model->find($loanTermId);
	}
}