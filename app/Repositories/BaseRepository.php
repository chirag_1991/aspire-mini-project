<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
	/**
	* @var $model
	*/
	protected $model;

	/**
	 * BaseRepository Constructor
	 *
	 * @param Loan $model
	*/
	public function __construct(Model $model)
	{
		$this->model = $model;
	}
}