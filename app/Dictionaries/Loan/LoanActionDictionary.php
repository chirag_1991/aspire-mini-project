<?php

namespace App\Dictionaries\Loan;

use App\Dictionaries\BaseDictionary;

/**
 * class LoanActionDictionary
 *
 * @package App\Dictionaries\Loan
 */
final class LoanActionDictionary extends BaseDictionary
{
	public const APPROVE = 'approve';
	public const REJECT = 'reject';
	public const PENDING = 'pending';
	public const COMPLETED = 'completed';
}