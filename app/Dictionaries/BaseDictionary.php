<?php

namespace App\Dictionaries;

/**
 * class BaseDictionary
 *
 * @package App\Dictionaries
 */
abstract class BaseDictionary
{
	
	/**
	* Returns the list of constant values
	*
	* @return array
	* 
	* @throws \ReflectionException
	*/

	public static function getValues() : array
	{
		$refl = new \ReflectionClass(get_called_class());
		return array_values($refl->getConstants());
	}
}