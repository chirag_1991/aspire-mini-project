<?php

namespace App\Services\Loan;

use App\Exceptions\InvalidDataException;
use App\Repositories\Loan\LoanRepository;
use App\Repositories\Loan\LoanTermRepository;
use App\Repositories\Loan\LoanRepaymentRepository;
use App\Dictionaries\Loan\LoanActionDictionary;
use Illuminate\Support\Carbon;
use App\Models\Loan;

use DateTime;
use DateInterval;
use DatePeriod;
/**
 * class LoanServiceImpl
 *
 * @package App\Services\Loan
 */
class LoanServiceImpl implements LoanService
{
	
	/**
     * @var LoanRepository
     */
    private $loanRepository;

    /**
     * @var LoanTermRepository
     */
    private $loanTermRepository;

    /**
     * @var LoanRepaymentRepository
     */
    private $loanRepaymentRepository;

    /**
     * LoanServiceImpl constructor.
     *
     * @param LoanRepository $loanRepository ,
     */
    public function __construct(LoanRepository $loanRepository,
    	LoanTermRepository $loanTermRepository,
    	LoanRepaymentRepository $loanRepaymentRepository
    )
    {
        $this->loanRepository = $loanRepository;
        $this->loanTermRepository = $loanTermRepository;
        $this->loanRepaymentRepository = $loanRepaymentRepository;
    }

	/**
	* This method is used to apply for loan
	*
	* @param array $data
	*
	* @return mixed
	*/
	public function applyForLoan(array $data) 
	{
		try{
			$userId = auth()->user()->id;
			$isEligible = $this->isEligible($userId);

			if(!$isEligible){
				throw new InvalidDataException("Error Processing Request. Please contact support");
			}

			$data['user_id'] = $userId;
			$data['repayment_frequency'] = $data['repayment_frequency'] ?? config('loan.repayment_frequency');
			$data['status'] = LoanActionDictionary::PENDING;
			$response = $this->loanRepository->saveLoanData($data);
			return true;
			
		} catch(\Exception $e){
			throw new InvalidDataException("Error Processing Request : ". $e->getMessage());
		}
	}


	/**
	* This method will validate if user has any pending loan request
	*
	* @param int $userId
	*
	* @return mixed
	*/
	public function isEligible(int $userId) 
	{
		
		$collection = $this->loanRepository->fetchActivePendingRequest($userId);

		return $collection->isEmpty();
	}


	/**
	* This method is list the loan requested by user
	*
	* @return mixed
	*/
	public function getUserLoans() 
	{
		try{
			$userId = auth()->user()->id;
			return $this->loanRepository->getUserLoans($userId);
			
		} catch(\Exception $e){
			throw new InvalidDataException("Error Processing Request : ". $e->getMessage());
		}
	}


	/**
	* This method is to update the loan status
	*
	* @param int $loanId	
	* @param array $data
	*
	* @return mixed
	*/
	public function loanAction(int $loanId, array $data) 
	{
		try{
			$userId = auth()->user()->id;

			//generate the reschedule payment in case of approved
			if($data['action'] == LoanActionDictionary::APPROVE){
				$this->calculateLoanRepayments($loanId);
			}
			return $this->loanRepository->updateLoanStatus($loanId, $data);
			
		} catch(\Exception $e){
			throw new InvalidDataException("Error Processing Request : ". $e->getMessage());
		}
	}

	/**
	* This method is to calculate and store the loan repayments
	*
	* @param int $loanId
	*
	* @return mixed
	*/
	public function calculateLoanRepayments(int $loanId) 
	{
		try {
			$loan = $this->loanRepository->getLoan($loanId);
			$loan = $loan->first();

			$intervals = config('loan.repayment_frequency_internal');;
			$loanTerm = $this->loanTermRepository->getLoanTerm($loan->loan_term_id);
			// loan term
		    $requestDate = Carbon::now();
		    $requestDate->add($loanTerm->number, $loanTerm->term_type);

		    $intvl = $intervals[$loan->repayment_frequency] ?? config('loan.default_repyament_frequency');
		    // loan re-pqyment table entry
		    $start    = new DateTime(); // Today date
		    $end      = new DateTime($requestDate->toDateTimeString()); // Create a datetime object from your Carbon object
		    $interval = DateInterval::createFromDateString($intvl); //interval
		    $period   = new DatePeriod($start, $interval, $end); // Get a set of date beetween the 2 period

		    $months = array();
		    $noOfPayments = 0;
		    foreach ($period as $dt) {
		      $paymentDate = Carbon::parse($dt)->addDays(config('loan.loan_start_date_offset'))->format('Y-m-d h:i:s');
		        $months[] = $paymentDate;
		        $noOfPayments++;
		    }
		    
		   	$this->storeLoanRepayment($loan, $months, $noOfPayments);

		} catch (\Exception $e){
			throw new InvalidDataException("Error Processing Request calculating loan repayments: ". $e->getMessage());
		}
	}


	/**
	* This method is to store the loan repayments
	*
	* @param Loan $loan
	* @param array $months
	* @param int $noOfPayments
	*
	* @return mixed
	*/
	public function storeLoanRepayment(Loan $loan, array $months, int $noOfPayments = 1) 
	{
		try {

			$interestRate = config('loan.interest_rate'); 
		   	$totalAmountWithInterest = $loan->loan + ($loan->loan * $interestRate / 100); //calculate total amount with interest.
		   	$amountRepayble = round($totalAmountWithInterest / $noOfPayments, 2);
		   	
		   	foreach ($months as $value) {
		   		$data['loan_id'] = $loan->id;
		   		$data['due_date'] = $value;
		   		$data['amount_paid'] = $amountRepayble;

		   		$this->loanRepaymentRepository->createLoanRepayments($data);
		   	}

		   	return true;

		} catch (\Exception $e){
			throw new InvalidDataException("Error Processing Request storing loan repayments: ". $e->getMessage());
		}
	}



	/**
	* This methos is used for loan repayment
	*
	* @param int $loanId	
	*
	* @return mixed
	*/
	public function loanRepayment(Int $loanId)
	{

		try {
			$userId = auth()->user()->id;
			$loan = $this->loanRepository->getUserLoanById($loanId, $userId);

			if($loan){
				$data['interest_rate'] = config('loan.interest_rate');
				
				$loanRepayment = $this->loanRepaymentRepository->updateLoanRepayment($loan->id, $data);
				if($loanRepayment) {

					$amounts = [];
					$amounts['amount_paid'] = $loanRepayment->amount_paid;
					$this->loanRepository->updateLoanAmount($loan->id, $amounts);
				} else {
					
					$loan->status = LoanActionDictionary::COMPLETED;
					$loan->save();
				}
			} else {
				throw new \Exception("Error Processing Request paying loan repayments: Invalid Request");
			}
			
		} catch (\Exception $e){
			throw new InvalidDataException("Error Processing Request paying loan repayments: ". $e->getMessage());
		}
	}
}