<?php

namespace App\Services\Loan;

/**
 * interface LoanService
 *
 * @package App\Services\Loan
 */
interface LoanService
{
	
	/**
	* This method is used to apply for loan
	*
	* @param array $data
	*
	* @return mixed
	*/
	public function applyForLoan(array $data);


	/**
	* This method is list the loan requested by user
	*
	* @return mixed
	*/
	public function getUserLoans();


	/**
	* This method is to update the loan status
	*
	* @param int $loanId	
	* @param array $data
	*
	* @return mixed
	*/
	public function loanAction(int $loanId, array $data);


	/**
	* This methos is used for loan repayment
	*
	* @param int $loanId		
	*
	* @return mixed
	*/
	public function loanRepayment(Int $loanId);
}