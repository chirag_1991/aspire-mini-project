<?php

namespace App\Http\Controllers\Loan;

use App\Http\Controllers\Controller;
use App\Services\Loan\LoanService;
use App\Http\Request\Loan\LoanRequest;
use App\Utils\ResponseUtils;
use Illuminate\Http\JsonResponse;
use App\Http\Request\Loan\LoanActionRequest;

/**
 * class LoanController
 *
 * @package App\Http\Controllers\Loan
 */
class LoanController extends Controller
{

	/**
	* @var $loanService
	*/
	private $loanService;

	/**
	* LoanController Constructor
	*
	* @param LoanService $loanService	
	*/
	public function __construct(LoanService $loanService)
	{
		$this->loanService = $loanService;
	}


	/**
	* This methos is used for apply a loan
	*
	* @param Request $request	
	*
	* @return JsonResponse
	*/
	public function applyForLoan(LoanRequest $request)
	{
		$response = $this->loanService->applyForLoan($request->validated());

		return ResponseUtils::successWithMessage('You have successfully applied for a loan!');
	}


	/**
	* This methos is used get users loans
	*
	* @param Request $request
	*
	* @return JsonResponse	
	*/
	public function userLoans()
	{
		$response = $this->loanService->getUserLoans();
		return ResponseUtils::successWithData($response);
	}


	/**
	* This methos is used update the status of the applied loan
	*
	* @param int $loanId	
	* @param LoanActionRequest $loanActionRequest
	*
	* @return JsonResponse
	*/
	public function loanAction(int $loanId, LoanActionRequest $loanActionRequest)
	{
		$response = $this->loanService->loanAction($loanId, $loanActionRequest->validated());
		return ResponseUtils::successWithMessage('Loan status updates successfully');
	}


	/**
	* This methos is used for loan repayment
	*
	* @param int $loanId	
	*
	* @return JsonResponse
	*/
	public function loanRepayment(Int $loanId)
	{
		$response = $this->loanService->loanRepayment($loanId);

		return ResponseUtils::successWithMessage('Thank you for the repayment');
	}

}