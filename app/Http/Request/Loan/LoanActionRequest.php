<?php

namespace App\Http\Request\Loan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Dictionaries\Loan\LoanActionDictionary;

/**
 * class LoanActionRequest
 *
 * @package App\Http\Request\Loan
 */
class LoanActionRequest extends FormRequest
{
	/**
	* Determine if the user is authorized to make this request.
	*
	* @retrum bool
	*/
	public function autorize() : bool
	{
		return true;
	}

	/**
	* Get the validation rule that apply to this request.
	*
	* @retrum array
	*/
	public function rules() : array 
	{
		return [
			'action' => 'required|' . Rule::in(LoanActionDictionary::getValues())
		];
	}

	public function validationData()
	{	
		return $this->post();
	}
}