<?php

namespace App\Http\Request\Loan;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * class LoanRequest
 *
 * @package App\Http\Request\Loan
 */
class LoanRequest extends FormRequest
{
	/**
	* Determine if the user is authorized to make this request.
	*
	* @retrum bool
	*/
	public function autorize() : bool
	{
		return true;
	}

	/**
	* Get the validation rule that apply to this request.
	*
	* @retrum array
	*/
	public function rules() : array 
	{
		return [
			'loan' => 'required|numeric|min:' . config('loan.minimum_loan_amount'),
			'loan_term_id' => 'required|integer|exists:loan_terms,id'
		];
	}

	public function validationData()
	{	
		return $this->post();
	}
}