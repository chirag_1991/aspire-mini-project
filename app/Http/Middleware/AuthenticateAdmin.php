<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

/**
 * Class AuthenticateAdmin
 *
 * @package App\Http\Middleware
 */
class AuthenticateAdmin extends Middleware
{
	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	//asuming this to be an admin
        if (auth()->user()->id == 1) {
            return $next($request);
        }
        return response()->json('Unauthorized');

    }
}