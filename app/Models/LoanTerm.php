<?php

namespace App\Models;

/**
* Class LoanTerm
* @Package App\Models
*/
class LoanTerm extends BaseModel
{
    
    /**
    * @var $table
    */
    protected $table = 'loan_terms';

}
