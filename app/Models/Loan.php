<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
* Class Loan
* @Package App\Models
*/
class Loan extends BaseModel
{
    use HasFactory;
    /**
    * @var $table
    */
    protected $table = 'loans';


    protected $casts = [
        'loan' => 'float',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fetch loan repayments for the given loan
     *
    */
    public function loanRepayments()
    {
        return $this->hasMany('App\Models\LoanRepayment','loan_id','id');
    }

}
