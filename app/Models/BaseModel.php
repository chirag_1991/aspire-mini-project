<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
* Class BaseModel
* @Package App\Models
*/

Class BaseModel extends Model
{
	/**
	* @var $table
	*/
	protected $table;

	/**
	* @var $guarded
	*/
	protected $guarded = ['id']; 
}