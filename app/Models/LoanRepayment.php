<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
* Class LoanRepayment
*
* @Package App\Models
*/
class LoanRepayment extends BaseModel
{
    use HasFactory;
    /**
    * @var $table
    */
    protected $table = 'loan_repayments';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
