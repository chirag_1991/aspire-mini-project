<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->decimal('loan', $precision = 10, $scale = 2);
            $table->unsignedTinyInteger('loan_term_id');
            $table->decimal('total_interest_paid', $precision = 6, $scale = 2)->nullable()->default(null);
            $table->decimal('total_amount_paid', $precision = 10, $scale = 2)->nullable()->default(null);
            $table->string('repayment_frequency', 10);
            $table->unsignedBigInteger('action_user_id')->nullable()->default(null);
            $table->string('status', 10);
            $table->string('comment', 1000)->nullable()->default(null);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
