<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_repayments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loan_id');
            $table->date('due_date');
            $table->decimal('interest_rate', $precision = 6, $scale = 2)->nullable()->default(null);
            $table->decimal('amount_paid', $precision = 10, $scale = 2)->nullable()->default(null);
            $table->decimal('interest_paid', $precision = 6, $scale = 2)->nullable()->default(null);    
            $table->boolean('is_paid')->default(false);            
            $table->timestamps();

            $table->foreign('loan_id')->references('id')->on('loans')->onUpdate('cascade')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_repayments');
    }
}
