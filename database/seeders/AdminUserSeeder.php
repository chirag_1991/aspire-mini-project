<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'id' => 1,
        		'name' => 'Admin',
        		'email' => 'admin@root.com',
        		'password' => $password = Hash::make('admin@123'),
        	]
        ];

        foreach ($data as $dt) {
        	User::updateOrCreate(['id' => $dt['id']], 
        		[
        			'name' => $dt['name'],
        			'email' => $dt['email'],
        			'password' => $dt['password'],
        		]);
        }
        
    }
}
