<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\LoanTerm;

class LoanTermSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'id' => 1,
        		'display_name' => '1 Month',
        		'number' => 1,
        		'term_type' => 'months'
        	],
        	[
        		'id' => 2,
        		'display_name' => '6 Months',
        		'number' => 6,
        		'term_type' => 'months'
        	],
        	[
        		'id' => 3,
        		'display_name' => '1 Year',
        		'number' => 1,
        		'term_type' => 'years'
        	],
        	[
        		'id' => 4,
        		'display_name' => '3 Years',
        		'number' => 3,
        		'term_type' => 'years'
        	],
        	[
        		'id' => 5,
        		'display_name' => '5 Years',
        		'number' => 5,
        		'term_type' => 'years'
        	],
        	[
        		'id' => 6,
        		'display_name' => '7 Years',
        		'number' => 7,
        		'term_type' => 'years'
        	],
        	[
        		'id' => 7,
        		'display_name' => '10 Years',
        		'number' => 10,
        		'term_type' => 'years'
        	]
        ];

        foreach ($data as $dt) {
        	LoanTerm::updateOrCreate(['id' => $dt['id']], 
        		[
        			'display_name' => $dt['display_name'],
        			'number' => $dt['number'],
        			'term_type' => $dt['term_type'],
        		]);
        }
        
    }
}
