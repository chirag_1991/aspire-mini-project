<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Loan;
use App\Models\LoanRepayment;


class LoanRepaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'loan_id' => Loan::all()->random()->id,
            'due_date' => date('Y-m-d h:i:s'),
            'is_paid' => false
        ];
    }
}
