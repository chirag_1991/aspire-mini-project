<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\LoanTerm;


class LoanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'loan' => round($this->faker->randomFloat(10, $min = 25000, $max = 30000), 2),
            'loan_term_id' => LoanTerm::all()->random()->id,
            'status' => 'pending', // password
            'repayment_frequency' => 'week',
        ];
    }
}
