<?php

return [

	'minimum_loan_amount' => 25000,
	'repayment_frequency' => 'week',

	'default_repyament_frequency' => '1 week',

	'repayment_frequency_internal' => [
		'week' => '1 week'
	],

	'loan_start_date_offset' => 3,
	'interest_rate' => 3.25
];