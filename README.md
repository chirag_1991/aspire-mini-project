# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* It is an app that allows authenticated users to go through a loan application. It doesn’t have to contain too many fields, but at least “amount
* required” and “loan term.” All the loans will be assumed to have a “weekly” repayment frequency.
* After the loan is approved, the user must be able to submit the weekly loan repayments. It can be a simplified repay functionality, which won’t
* need to check if the dates are correct but will just set the weekly amount to be repaid

### How do I get set up? ###

* make sure you have php >= 7.4
* make sure you have MySQL >= 5.7
* Need to make following changes inside .env file 
	DB_CONNECTION=mysql
	DB_HOST=mysql_host
	DB_PORT=mysql_port
	DB_DATABASE=mysql_DB_NAME
	DB_USERNAME=mysql_username
	DB_PASSWORD=mysql_password
	
* please run the ./run.sh file to complete the setup and install the dependencies.
