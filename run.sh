composer install
php artisan key:generate
php artisan config:cache
sudo chmod -Rf 777 storage/
php artisan migrate
php artisan db:seed
./vendor/bin/phpunit --testdox